﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzamlaSzamOlvaso
{
    class SzamBeolvas
    {

        public void SzamlaSzamBeolvas()
        {


            //using (StreamReader sr = new StreamReader(@"D:\\2019 Tavaszi félév\\MinosegbiztositasInformatikaja\\RiportFile\\BankSzamlaParszolas\\szamlaszamok.txt"))
            //{
            //    // Read the stream to a string, and write the string to the console.
            //    String line = sr.ReadToEnd();
            //    Console.WriteLine(line);
            //}

            List<String> lines = new List<String>();

            int lineCounter = 0;
            string line;

            // Read the file and display it line by line.  
            StreamReader file = new StreamReader(@"D:\\2019 Tavaszi félév\\MinosegbiztositasInformatikaja\\RiportFile\\BankSzamlaParszolas\\szamlaszamok.txt");
            while ((line = file.ReadLine()) != null)
            {
                if (line != "")
                {
                    lines.Add(line);
                }

                lineCounter++;
            }

            file.Close();

            List<char[]> allNumbers = new List<char[]>();

            int e;
            for (e = 0; e < lines.Count; e+=3)
            {

                List<string> numbers = new List<string>();

                int counterToThree = 0;

                for (int i = e; i < e + 3; i++)
                {
                    char[] charArray = lines[i].ToCharArray();
                    List<char> threeChars = new List<char>();

                    for (int j = 0; j < charArray.Length; j++)
                    {

                        if ((counterToThree == 2) || (counterToThree == 0))
                        {
                            threeChars.Add(charArray[j]);
                            if (counterToThree == 0)
                            {
                                counterToThree++;

                            }
                            else if (counterToThree == 2)
                            {

                                string threeCharString = string.Join("", threeChars.ToArray());
                                numbers.Add(threeCharString);
                                threeChars.Clear();
                                counterToThree = 0;
                            }
                        }
                        else
                        {
                            threeChars.Add(charArray[j]);
                            counterToThree++;

                        }
                    }

                }

                for (int i = 0; i < numbers.Count; i++)
                {
                    //Console.WriteLine(i + ". hármas: " + numbers[i]);
                }

                List<string[]> numberStringList = new List<string[]>();
                List<string> numberString = new List<string>();

                int counter = 0;

                for (int k = 0; k < numbers.Count; k++)
                {
                    for (int i = k; i < numbers.Count; i += 9)
                    {
                        numberString.Add(numbers[i]);
                        if (counter == 2)
                        {
                            numberStringList.Add(numberString.ToArray());
                            numberString.Clear();
                        }
                        counter++;

                    }
                    counter = 0;
                    if (numberStringList.Count() == 9)
                    {
                        break;
                    }
                }


                Szamok alapSzamok = new Szamok();

                List<char> finalNumbers = new List<char>();
                for (int i = 0; i < numberStringList.Count; i++)
                {
                    string compared = string.Join("", numberStringList[i]);
                    if (compared.Equals(alapSzamok.Nulla))
                    {
                        finalNumbers.Add('0');
                    }
                    else if (compared.Equals(alapSzamok.Egy))
                    {
                        finalNumbers.Add('1');
                    }
                    else if (compared.Equals(alapSzamok.Ketto))
                    {
                        finalNumbers.Add('2');
                    }
                    else if (compared.Equals(alapSzamok.Harom))
                    {
                        finalNumbers.Add('3');
                    }
                    else if (compared.Equals(alapSzamok.Negy))
                    {
                        finalNumbers.Add('4');
                    }
                    else if (compared.Equals(alapSzamok.Ot))
                    {
                        finalNumbers.Add('5');
                    }
                    else if (compared.Equals(alapSzamok.Hat))
                    {
                        finalNumbers.Add('6');
                    }
                    else if (compared.Equals(alapSzamok.Het))
                    {
                        finalNumbers.Add('7');
                    }
                    else if (compared.Equals(alapSzamok.Nyolc))
                    {
                        finalNumbers.Add('8');
                    }
                    else if (compared.Equals(alapSzamok.Kilenc))
                    {
                        finalNumbers.Add('9');
                    }
                    else
                    {
                        finalNumbers.Add('?');
                    }
                }

                allNumbers.Add(finalNumbers.ToArray());

            }



            string path = @"D:\\2019 Tavaszi félév\\MinosegbiztositasInformatikaja\\RiportFile\\BankSzamlaParszolas\\repost.txt";


            if (File.Exists(path))
            {
                File.Delete(path);
            }
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {


                    for (int i = 0; i < allNumbers.Count; i++)
                    {

                        if (allNumbers[i].Contains('?'))
                        {
                            sw.WriteLine(string.Join("", allNumbers[i]) + " ERR");
                            Console.WriteLine("A számlaszám: " + string.Join("", allNumbers[i]) + " ERR");

                        }
                        else {
                            int sum = 0;

                            for (int j = 0; j < allNumbers[i].Length; j++)
                            {
                                if(j == 0)
                                {
                                    sum += (int)char.GetNumericValue(allNumbers[i][j]);
                                }else if(j == 1)
                                {
                                    sum += 2 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 2)
                                {
                                    sum += 3 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 3)
                                {
                                    sum += 4 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 4)
                                {
                                    sum += 5 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 5)
                                {
                                    sum += 6 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 6)
                                {
                                    sum += 7 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 7)
                                {
                                    sum += 8 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                                else if(j == 8)
                                {
                                    sum += 9 * (int)char.GetNumericValue(allNumbers[i][j]);
                                }
                            }

                            if(sum%11 != 0)
                            {
                                sw.WriteLine(string.Join("", allNumbers[i]) + " ILL");
                                Console.WriteLine("A számlaszám: " + string.Join("", allNumbers[i]) + " ILL");

                            }
                            else if(sum % 11 == 0)
                            {
                                sw.WriteLine(string.Join("", allNumbers[i]));
                                Console.WriteLine("A számlaszám: " + string.Join("", allNumbers[i]));

                            }
                        }
                        

                    }




                }
            }

            

        }
    }
}
