﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzamlaSzamOlvaso
{
    class Szamok
    {
        private string nulla = " _ " +
                               "| |" +
                               "|_|";

        private string egy = "   " +
                             " | " +
                             " | ";

        private string ketto = " _ " +
                               " _|" +
                               "|_ ";

        private string harom = " _ "+
                               " _|"+
                               " _|";

        private string negy = "   "+
                              "|_|"+
                              "  |";

        private string ot = " _ "+
                            "|_ "+
                            " _|" ;

        private string hat = " _ "+
                             "|_ "+
                             "|_|" ;

        private string het = " _ "+
                             "  |"+
                             "  |" ;

        private string nyolc = " _ "+
                               "|_|"+
                               "|_|";

        private string kilenc = " _ "+
                                "|_|"+
                                " _|";

        public string Nulla { get { return nulla; } }
        public string Egy { get { return egy; } }
        public string Ketto { get { return ketto; } }
        public string Harom { get { return harom; } }
        public string Negy { get { return negy; } }
        public string Ot { get { return ot; } }
        public string Hat { get { return hat; } }
        public string Het { get { return het; } }
        public string Nyolc { get { return nyolc; } }
        public string Kilenc { get { return kilenc; } }

    }
}
